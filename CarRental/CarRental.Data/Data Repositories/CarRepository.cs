﻿using CarRental.Business.Entities;
using CarRental.Data.Contracts.Repository_Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Data.Data_Repositories
{
    [Export(typeof(ICarRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CarRepository : DataRepositoryBase<Car>, ICarRepository
    {
        protected override Car AddEntity(CarRentalContext entityContext, Car entity)
        {
            return entityContext.CarSet.Add(entity);
        }

        protected override Car UpdateEntity(CarRentalContext entityContext, Car entity)
        {
            return entityContext.CarSet.FirstOrDefault(e => e.CarId == entity.CarId);
        }

        protected override IEnumerable<Car> GetEntities(CarRentalContext entityContext)
        {
            return entityContext.CarSet;
        }

        protected override Car GetEntity(CarRentalContext entityContext, int id)
        {
            return entityContext.CarSet.FirstOrDefault(e => e.CarId == id);
        }
    }
}
