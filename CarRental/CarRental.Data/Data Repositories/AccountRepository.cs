﻿using CarRental.Business.Entities;
using CarRental.Data.Contracts.Repository_Interfaces;
using System.ComponentModel.Composition;
using Core.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Data.Data_Repositories
{
    [Export(typeof(IAccountRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class AccountRepository : DataRepositoryBase<Account>, IAccountRepository
    {
        protected override Account AddEntity(CarRentalContext entityContext, Account entity)
        {
            return entityContext.AccountSet.Add(entity);
        }

        protected override IEnumerable<Account> GetEntities(CarRentalContext entityContext)
        {
            return entityContext.AccountSet;
        }

        protected override Account GetEntity(CarRentalContext entityContext, int id)
        {
            return entityContext.AccountSet.FirstOrDefault(e => e.AccountId == id);
        }

        protected override Account UpdateEntity(CarRentalContext entityContext, Account entity)
        {
            return entityContext.AccountSet.FirstOrDefault(e => e.AccountId == entity.AccountId);
        }

        public Account GetByLogin(string login)
        {
            using (CarRentalContext context = new CarRentalContext())
            {
                return context.AccountSet.FirstOrDefault(e => e.LoginEmail == login);
            }
        }
    }
}
