﻿using Core.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Client.Entities
{
    public class Rental : ObjectBase
    {
        int _RentalId;
        int _AccountId;
        int _CarId;
        DateTime _DateRented;
        DateTime _DueDate;
        DateTime? _DateReturned;

        public int RentalId
        {
            get { return _RentalId; }
            set
            {
                _RentalId = value;
                OnPropertyChanged(() => RentalId);
            }
        }

        public int AccountId
        {
            get { return _AccountId; }
            set
            {
                _AccountId = value;
                OnPropertyChanged(() => AccountId);
            }
        }

        public int CarId
        {
            get { return _CarId; }
            set
            {
                _CarId = value;
                OnPropertyChanged(() => CarId);
            }
        }

        public DateTime DateRented
        {
            get { return _DateRented; }
            set
            {
                _DateRented = value;
                OnPropertyChanged(() => DateRented);
            }
        }

        public DateTime DueDate
        {
            get { return _DueDate; }
            set
            {
                _DueDate = value;
                OnPropertyChanged(() => DueDate);
            }
        }

        public DateTime? DateReturned
        {
            get { return _DateReturned; }
            set
            {
                _DateReturned = value;
                OnPropertyChanged(() => DateReturned);
            }
        }
    }
}
