﻿using Core.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Client.Entities
{
    public class Reservation : ObjectBase
    {
        int _ReservationId;
        int _AccountId;
        int _CarId;
        DateTime _RentalDate;
        DateTime _ReturnDate;

        public int ReservationId
        {
            get { return _ReservationId; }
            set
            {
                _ReservationId = value;
                OnPropertyChanged(() => ReservationId);
            }
        }

        public int AccountId
        {
            get { return _AccountId; }
            set
            {
                _AccountId = value;
                OnPropertyChanged(() => AccountId);
            }
        }

        public int CarId
        {
            get { return _CarId; }
            set
            {
                _CarId = value;
                OnPropertyChanged(() => CarId);
            }
        }

        public DateTime RentalDate
        {
            get { return _RentalDate; }
            set
            {
                _RentalDate = value;
                OnPropertyChanged(() => RentalDate);
            }
        }

        public DateTime ReturnDate
        {
            get { return _ReturnDate; }
            set
            {
                _ReturnDate = value;
                OnPropertyChanged(() => ReturnDate);
            }
        }
    }
}
