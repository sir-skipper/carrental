﻿using Core.Common.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Client.Entities
{
    public class Car : ObjectBase
    {
        int _CarId;
        private string _Description;
        private string _Color;
        private int _Year;
        private decimal _RentalPrice;
        private bool _CurrentlyRented;

        public int CarId
        {
            get { return _CarId; }
            set
            {
                _CarId = value;
                OnPropertyChanged(() => CarId);
            }
        }

        public string Description
        {
            get { return _Description; }
            set
            {
                _Description = value;
                OnPropertyChanged(() => Description);

            }
        }

        public string Color
        {
            get { return _Color; }
            set
            {
                _Color = value;
                OnPropertyChanged(() => Color);

            }
        }

        public int Year
        {
            get { return _Year; }
            set
            {
                _Year = value;
                OnPropertyChanged(() => Year);
            }
        }

        public decimal RentalPrice
        {
            get { return _RentalPrice; }
            set
            {
                _RentalPrice = value;
                OnPropertyChanged(() => RentalPrice);
            }
        }

        public bool CurrentlyRented
        {
            get { return _CurrentlyRented; }
            set
            {
                _CurrentlyRented = value;
                OnPropertyChanged(() => CurrentlyRented);
            }
        }

        class CarValidator : AbstractValidator<Car>
        {
            public CarValidator()
            {
                RuleFor(obj => obj.Description).NotEmpty();
                RuleFor(obj => obj.Color).NotEmpty();
                RuleFor(obj => obj.RentalPrice).GreaterThan(0);
                RuleFor(obj => obj.Year).GreaterThan(2000).LessThanOrEqualTo(DateTime.Now.Year);                
            }
        }

        protected override IValidator GetValidator()
        {
            return new CarValidator();
        }
    }
}
