﻿using Core.Common.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.Client.Entities
{
    public class Account : ObjectBase
    {
        int _AccountId;
        string _LoginEmail;
        string _FirstName;
        string _LastName;
        string _Address;
        string _City;
        string _State;
        string _ZipCode;
        string _CreditCard;
        string _ExpDate;

        public int AccountId
        {
            get { return _AccountId; }
            set
            {
                _AccountId = value;
                OnPropertyChanged(() => AccountId);
            }
        }

        public string LoginEmail
        {
            get { return _LoginEmail; }
            set
            {
                _LoginEmail = value;
                OnPropertyChanged(() => LoginEmail);
            }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                _FirstName = value;
                OnPropertyChanged(() => FirstName);
            }
        }

        public string LastName
        {
            get { return _LastName; }
            set
            {
                _LastName = value;
                OnPropertyChanged(() => LastName);
            }
        }

        public string Address
        {
            get { return _Address; }
            set
            {
                _Address = value;
                OnPropertyChanged(() => Address);
            }
        }

        public string City
        {
            get { return _City; }
            set
            {
                _City = value;
                OnPropertyChanged(() => City);
            }
        }

        public string State
        {
            get { return _State; }
            set
            {
                _State = value;
                OnPropertyChanged(() => State);
            }
        }

        public string ZipCode
        {
            get { return _ZipCode; }
            set
            {
                _ZipCode = value;
                OnPropertyChanged(() => ZipCode);
            }
        }

        public string CreditCard
        {
            get { return _CreditCard; }
            set
            {
                _CreditCard = value;
                OnPropertyChanged(() => CreditCard);
            }
        }

        public string ExpDate
        {
            get { return _ExpDate; }
            set
            {
                _ExpDate = value;
                OnPropertyChanged(() => ExpDate);
            }
        }
    }
}
