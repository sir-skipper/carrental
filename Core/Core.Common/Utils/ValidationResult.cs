﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common.Utils
{
    public class ValidationResult
    {
        public IEnumerable<ValidationFailure> Errors { get; set; }
    }
}
