﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Common.Extensions
{
    public static class ObjectExtensions
    {
        public static T[] ToFullyLoaded<T>(this IQueryable<T> query)
        {
            return query.ToList().ToArray();
        }
    }
}
